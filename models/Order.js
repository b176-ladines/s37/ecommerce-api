//[SECTION] Modules and Dependencies	
	const mongoose = require('mongoose');

//[SECTION]	Schema/Blueprint
	const orderSchema = new mongoose.Schema({
	 
	userId: {
            type: String,
            required: [true, "User ID is required"]
        },

        products: [
            {
                productId: {
                    type: String,
                    required: [true, "Product ID is required"]
                },
                quantity: {
                    type: Number,
                    required: [true, "Quantity is Required"]

                }
            }
        ],

        totalAmount: {
            type: Number,
            default: 0
        },

        purchasedOn: {
            type: Date,
            default: new Date()
        }         
    });



//[SECTION]	Model
	module.exports = mongoose.model('Order', orderSchema);
