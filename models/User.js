//[SECTION] Modules and Dependencies
   const mongoose = require('mongoose');

//[SECTION] Schema/Blueprint 
	const userSchema = new mongoose.Schema({
		email: {
			type: String,
			required: [true, 'Email is Required']
		},
		password: {
			type: String,
			required: [true, 'Password is Required']
		},
		isAdmin: {
			type: Boolean,
			default: false
		},
		addToCart: [
				{
					productId: {
						type: String,
						required: [true, "Product must have an ID"],
					},
					quantity: {
						type: Number,
						default: 1,
					},
					dateAdded: {
						type: Date,
						default: new Date(),
					},
				},
			],

	});

//[SECTION] Model
	module.exports = mongoose.model('User', userSchema);