//[SECTION] Depedencies and Modules
const Order = require("../models/Order");
const Product = require("../models/Product");

//[SECTION] Functionalities [CREATE]
const getTotal = async (total, addCart) => {
	for (let i = 0; i < addCart.length; i++) {
		await Product.findById(addCart[i].productId).then((product) => {
				total += product.price * addCart[i].quantity;
			}).catch((err) => err.message);
	}
	return total;
};

//Non-admin User checkout (Create Order)

module.exports.createOrder = async (req, res) => {
	if (req.user.isAdmin) {
		return res.send({ message: "Action forbidden" });
	} else {
		let addCart = req.body;
		if (addCart.length) {			
			let total = 0;
			let newOrderDetails = {
				userId: req.user.id,
				totalAmount: await getTotal(total, addCart),
			};
			let newOrder = new Order(newOrderDetails);
			addCart.forEach((product) => {
				newOrder.products.push(product);
			});
			return newOrder.save().then((result) => res.send(result)).catch((err) => 
				res.send(err.message));
		} else {
			return Product.findById(addCart.productId).then((product) => {
					if (product.isActive === false) {
						return new Promise((resolve, error) => {
							error({ message: "Product is not active" });
						});
					} else {
						let total = addCart.quantity * product.price;
						let newOrderDetails = {
							userId: req.user.id,
							totalAmount: total,
						};
						let newOrder = new Order(newOrderDetails);
						newOrder.products.push(addCart);
						return newOrder.save().then((result) => res.send(result)).catch((err) => 
							res.send(err.message));
					}
				}).catch((err) => res.send(err.message));
		}
	}
};

//[STRETCH GOALS]

//Retrieve authenticated user’s orders
module.exports.getUsersOrder = (id) => {
    return Order.find({
        userId: id
    }).then((orders, error) => {
        if(error)
        {
            return error
        }

        return orders
    }).catch(error => {
        return error.message
    })
}


// Retrieve all orders (Admin only)
module.exports.getAllOrders = async (req, res) => {
	return Order.find({}).then((result) => res.send(result)).catch((err) => res.send(err.message));
};
