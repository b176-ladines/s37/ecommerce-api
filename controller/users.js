//[SECTION] Depedencies and Modules
	const User = require('../models/User'); 
  const Order = require('../models/Order'); 
	const Product = require('../models/Product'); 
  const bcrypt = require('bcrypt');
	const dotenv = require('dotenv');
	const auth = require('../auth.js')

//[SECTION] Environment Variables Setup
	dotenv.config();
	const asin = Number(process.env.SALT);

//[SECTION] Functionalities [Create]
	//USER REGISTRATION
	module.exports.register = (userData) => {
        let email = userData.email;
        let passW = userData.password;
		
		
		let newUser = new User({
        	email: email,
        	password: bcrypt.hashSync(passW, asin) 
		});

		return newUser.save().then((user, err) => {
			if (user) {
				return user;
			} else {
				return {message: 'Failed to Register Account'};
			};
		}); 
	};



	//LOGIN/USER AUTHENTICATION

  	module.exports.loginUser = (data) => {
  		return User.findOne({ email: data.email }).then(result => {

  				if(result == null) {
  						return false;
  				} else {
  						const isPasswordCorrect = bcrypt.compareSync(data.password, result.password)

  						if(isPasswordCorrect){
  								return { accessToken: auth.createAccessToken(result.toObject()) }
  						}else {
  							return false;
  						}

  				}
  		}).catch((err) => err.message);
  	}

//Add to Cart
    module.exports.addToCart = async (userId, productInfo) => {
      let { productId, quantity } = productInfo;

      return Product.findById(productId).then((product) => {
          let newProduct = { productId: product.id, quantity };

          return User.findById(userId).then((user) => {
            user.addCart.push(newProduct);
            return user.save().then((result) => result)
              .catch((err) => err.message);
          });
        }).catch((err) => err.message);
    };




//[SECTION] - USERS DETAILS
   //Retrieve authenticated user profile
    module.exports.getUserDetails = (data) => {
      	return User.findById(data).then(result => {
      			result.password = '';
      			return result;
      	}).catch((err) => err.message);
      }

//[SECTION] - STRECH GOALS
   //Retrieve authenticated user’s orders
	module.exports.getUserOrders = (userId) => {
		return Order.find({ userId }).then((result) => result)
		.catch((err) => err.message);
	};

   	//Set user as admin (Admin only)

    module.exports.setAdmin = (userId) => {
     	return User.findByIdAndUpdate(userId, { isAdmin: true }).then(() => {
     		return { message: "User Updated to Admin Successfully!" };
     		}).catch((err) => err.message);
     	};
