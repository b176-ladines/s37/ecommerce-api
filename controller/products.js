//[SECTION] Depedencies and Modules
const Product = require("../models/Product");

//[SECTION] Functionalities [CREATE]
	//Add Product
	module.exports.createProduct = (req, res) => {

		console.log(req.body);

		let newProduct = new Product({

			name: req.body.name,
			description: req.body.description,
			price: req.body.price
		})

		newProduct.save()
		.then(product => res.send(product))
		.catch(err => res.send(err));
	};

	// Get Active Products
	module.exports.getActiveProducts = (req, res) => {

		Product.find({isActive: true})
		.then(result => res.send(result))
		.catch(err => res.send(err));
	};

	// Get Single Product
	module.exports.getSingleProduct = (req, res) => {

		console.log(req.params);
		
		Product.findById(req.params.id)
		.then(result => res.send(result))
		.catch(err => res.send(err));
	};

	// Update Product
	module.exports.updateProduct = (productId, data) => {
	let updatedProduct = {
		name: data.name,
		description: data.description,
		price: data.price
	}

	return Product.findByIdAndUpdate(productId, updatedProduct).then((course, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	}).catch(error => error)
}

	//Archiving a Product
	module.exports.archiveProduct = (productId) => {
		let updateActiveField = {
			isActive: false
		};

		return Product.findByIdAndUpdate(productId, updateActiveField).then((course, error) => {

			if(error){
				return false;
			} else {
				return true;
			}
		}).catch(error => error)
	}


