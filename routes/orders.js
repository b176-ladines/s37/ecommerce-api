//[SECTION] Dependencies and Modules
    const exp = require('express');
    const OrderController = require('../controller/orders');


//[SECTION] Routing Component

    const route = exp.Router();
    const auth = require("../auth");
    const {verify, verifyAdmin} = auth;


//[SECTION] Routes-POST
	//Non-admin User checkout (Create Order)
	// Create Order
	route.post('/createOrder', verify, OrderController.createOrder);



    //[STRETCH GOALS]

    //Retrieve authenticated user’s orders
    route.get('/usersOrder', verify, (request, response) => {
        OrderController.getUsersOrder(request.user.id).then(result => {
            response.send(result)
        })
    })


	// Retrieve all orders (Admin only)
	route.get("/getAllOrders", verify, verifyAdmin, OrderController.getAllOrders);





    module.exports = route;
