//[SECTION] Depedencies and Modules
	const exp = require('express'); 
	const UserController = require('../controller/users');
	const auth = require('../auth')

//[SECTION] Routing Component
	const route = exp.Router();
	const { verify, verifyAdmin } = auth;

//[SECTION] Routes- POST
	//Create New User
	route.post('/register', (req, res) => {
		console.log(req.body);
		let userData = req.body;
		UserController.register(userData).then(outcome => {
			res.send(outcome);
		});
	});

	//Login/
	route.post('/login', (req, res) => {
		let data = req.body;
		UserController.loginUser(data).then(outcome => {
			res.send(outcome)
		})
	});

//Add to Cart
	route.post("/addToCart", verify, (req, res) => {
		let productInfo = req.body;
		UserController.addToCart(req.user.id, productInfo).then((result) => res.send(result))
			.catch((err) => res.send(err));
	});


	//[SECTION] Routes- GET the user's details
	route.get('/details', verify, (req, res) => {
		UserController.getUserDetails(req.user.id).then(result => res.send(result));
	});
	

	//[SECTION] - STRECH GOALS

	//Set user as admin (Admin only)
	route.put("/:userId/admin", verify, verifyAdmin, (req, res) => {
		let userId = req.params.userId;
		UserController.setAdmin(userId).then((result) => res.send(result))
			.catch((err) => res.send(err.message));
	});

	

	//Retrieve user orders
	route.get("/getUserOrders", verify, (req, res) => {
		let userId = req.user.id;
		UserController.getUserOrders(userId).then((result) => res.send(result))
			.catch((err) => res.send(err.message));
	});













	module.exports = route;