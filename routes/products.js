//[SECTION] Depedencies and Modules
	const express = require("express");
	const ProductController = require("../controller/products");

//[SECTION] Routing Component
	const route = express.Router();
	const auth = require('../auth');

	const { verify, verifyAdmin } = auth;

//[SECTION] Routes- POST
	//CREATE PRODUCT-ADMIN
	route.post("/create", verify, verifyAdmin, ProductController.createProduct);

	//GET ALL ACTIVE PRODUCTS
	route.get("/getActiveProducts", ProductController.getActiveProducts);


//[SECTION] Routes- GET
	// Get Single Product
	route.get("/getSingleProduct/:id", ProductController.getSingleProduct);


//[SECTION] Routes- PUT
	// Update Product Information - Admin
	route.put('/:productId', verify, verifyAdmin, (req, res) => {
	ProductController.updateProduct(req.params.productId, req.body).then(result => res.send(result))
	})

	// Archive Product-Admin
	route.put('/:productId/archive', verify, verifyAdmin, (req, res) => {
		ProductController.archiveProduct(req.params.productId).then(result => res.send(result));
	})

























	module.exports = route;
